import pandas as pd
import mlflow
def logMetrics():
    metrics = {"mse": 2500.00, "rmse": 50.00}
    # Log a batch of metrics
    with mlflow.start_run():
        mlflow.log_metrics(metrics)
def column_merge(df1,df2):
    res=pd.concat([df1, df2], axis=1, join='inner')
    print(res.head(1))
def execute(df1,df2):
    logMetrics()
    column_merge(df1,df2)